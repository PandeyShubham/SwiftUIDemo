//
//  LandMarksApp.swift
//  SwiftUIDemo
//
//  Created by Mobcoder  on 17/08/23.
//

import SwiftUI

struct LandMarksApp: App {
    @StateObject private var modelData = ModelData()


    var body: some Scene {
        WindowGroup {
            ContentView()
                .environmentObject(modelData)
        }
    }
}
