//
//  SwiftUIDemoApp.swift
//  SwiftUIDemo
//
//  Created by Mobcoder  on 14/08/23.
//

import SwiftUI

@main
struct SwiftUIDemoApp: App {
    @StateObject private var modelData = ModelData()
    
    var body: some Scene {
        WindowGroup {
            ContentView()
                .environmentObject(modelData)
        }
    }
}
