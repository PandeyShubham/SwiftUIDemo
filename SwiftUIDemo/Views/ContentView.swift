//
//  ContentView.swift
//  SwiftUIDemo
//
//  Created by Mobcoder  on 14/08/23.
//

import SwiftUI

struct ContentView: View {
    @State private var selection: Tab = .featured
   
    
    enum Tab {
        case featured
        case list
        case anime
    }
    
    var body: some View {
        TabView(selection: $selection) {
            CategoryHome()
                .tabItem {
                    Label("Featured", systemImage: "star")
                }
                .tag(Tab.featured)
            
            LandMarkList()
                .tabItem {
                    Label("List", systemImage: "list.bullet")
                }
                .tag(Tab.list)
            AnimationPage()
                .tabItem {
                    Label("Anime", systemImage: "heart")
                }
                .tag(Tab.anime)
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView().environmentObject(ModelData())
    }
}
