//
//  AnimationPage.swift
//  SwiftUIDemo
//
//  Created by Mobcoder  on 28/08/23.
//

import SwiftUI

struct AnimationPage: View {
    var body: some View {
        ZStack{
            Color.gray.opacity(0.2)
                .ignoresSafeArea()
            
            ScrollView(showsIndicators: false) {
                VStack {
                    StepperAnimation().padding(.bottom, 30)
                    HeartAnimation().padding(.bottom, 30)
                    WaveAnimation().padding(.bottom, 30)
                    DotsAnimation().padding(.top, 100)
                    PacmanAnimation().padding(.top, 50)
                    TwinCircleAnimation().padding(.top, 50)
                }
            }
        }
    }
}

struct AnimationPage_Previews: PreviewProvider {
    static var previews: some View {
        AnimationPage()
    }
}
