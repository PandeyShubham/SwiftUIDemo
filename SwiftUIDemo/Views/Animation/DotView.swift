//
//  DotView.swift
//  SwiftUIDemo
//
//  Created by Mobcoder  on 28/08/23.
//

import SwiftUI

struct DotView: View {
    @Binding var transY: CGFloat
    
        var body: some View {
                VStack{}.frame(width: 40, height: 40, alignment: .center)
                    .background(Color.red)
                    .cornerRadius(20.0)
                    .offset(x: 0, y: transY)
        }
}
