//
//  PackManAnimation.swift
//  SwiftUIDemo
//
//  Created by Mobcoder  on 29/08/23.
//

import SwiftUI

struct PackManAnimation: View {
    var body: some View {
        Text(/*@START_MENU_TOKEN@*/"Hello, World!"/*@END_MENU_TOKEN@*/)
    }
}

struct PackManAnimation_Previews: PreviewProvider {
    static var previews: some View {
        PackManAnimation()
    }
}
