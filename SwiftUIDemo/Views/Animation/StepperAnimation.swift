//
//  StepperAnimation.swift
//  SwiftUIDemo
//
//  Created by Mobcoder  on 28/08/23.
//

import SwiftUI

struct StepperAnimation: View {
    @State var front: Int = 0
    @State var back: Int = 0
    @State var isFront = true
    @State private var animatedAngle = 0.0
    
    var body: some View {
        ZStack {
            Color.yellow
            ZStack {
                Text("\(isFront ? front : back)")
                    .font(.system(size: 25))
                    .rotation3DEffect(Angle(degrees: isFront ? 0 : 180),axis: (x: 0.0, y: 1.0, z: 0.0))
                HStack {
                    VStack {
                    }
                    .frame(width: 125, height: 100, alignment: .leading)
                    .contentShape(Rectangle())
                    .onTapGesture {
                        onFlipPrevious()
                    }
                   
                    VStack {
                    }
                    .frame(width: 125, height: 100, alignment: .trailing)
                    .contentShape(Rectangle())
                    .onTapGesture {
                        onFlipNext()
                    }
                }
            }
        }
        .frame(width: 250, height: 100, alignment: .center)
        .rotation3DEffect(Angle(degrees: isFront ? 0 : 180),axis: (x: 0.0, y: 1.0, z: 0.0))
        .animation(Animation.linear(duration: 0.6), value: isFront ? 0 : 180)
        .padding(.bottom, 30)
    }
    
    func onFlipPrevious() {
        if(isFront) {
                back = front - 1
            } else {
                front = back - 1
            }
       
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
                isFront = !isFront
            }
    }
    
    func onFlipNext() {
        if(isFront) {
                back = front + 1
            } else {
                front = back + 1
            }
           
            DispatchQueue.main.asyncAfter(deadline:  .now() + 0.3) {
                isFront = !isFront
            }
    }
}

struct StepperAnimation_Previews: PreviewProvider {
    static var previews: some View {
        StepperAnimation()
    }
}
