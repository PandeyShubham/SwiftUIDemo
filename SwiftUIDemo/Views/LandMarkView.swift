//
//  LandMarkView.swift
//  SwiftUIDemo
//
//  Created by Mobcoder  on 16/08/23.
//

import SwiftUI

struct LandMarkView: View {
    var landmark: Landmark
    
    var body: some View {
        HStack {
            landmark.image
                .resizable()
                .frame(width: 50, height: 50)
            
            Text(landmark.name)
            
            Spacer()
            
            
            if landmark.isFavorite {
                Image(systemName: "star.fill")
                    .foregroundColor(.yellow)
            }
        }
    }
}

struct LandMarkView_Previews: PreviewProvider {
    static var previews: some View {
        Group {
            LandMarkView(landmark: ModelData().landmarks[0])
                }
                .previewLayout(.fixed(width: 300, height: 70))
            }
}
