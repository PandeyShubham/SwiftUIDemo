//
//  LandMarkDetails.swift
//  SwiftUIDemo
//
//  Created by Mobcoder  on 16/08/23.
//

import SwiftUI

struct LandMarkDetails: View {
    var landmark: Landmark
    @EnvironmentObject var modelData: ModelData
    
    var landmarkIndex: Int {
            modelData.landmarks.firstIndex(where: { $0.id == landmark.id })!
        }
    
    var body: some View {
        ScrollView{
                MapView(coordinate: landmark.locationCoordinate)
                    .ignoresSafeArea(edges: .top)
                    .frame(height: 400)
                CircleImageView(image: landmark.image)
                    .offset(y: -130)
                    .padding(.bottom, -130)
                VStack(alignment:.leading) {
                    Text(landmark.name)
                        .font(.title).foregroundColor(.blue)
                        .fontWeight(.semibold)
                    HStack{
                        Text("MERN Developer")
                            .foregroundColor(.green)
                            .font(.title2)
                            .fontWeight(.semibold)
                        
                        FavoritesButton(isSet: $modelData.landmarks[landmarkIndex].isFavorite)
                        Spacer()
                        HStack{
                            Image(systemName: "phone.fill")
                            Text("+91 9876543210")
                        }
                    }
                    Divider()
                    HStack{
                        Text("Company Name: ")
                            .foregroundColor(.gray)
                            .font(.title2)
                        Spacer()
                        Text("Mobcoder")
                            .font(.title2)
                            .fontWeight(.medium)
                    }
                    HStack{
                        Text("Experience: ")
                            .foregroundColor(.gray)
                            .font(.title2)
                        Spacer()
                        Text("4+ Years")
                            .font(.title2)
                            .fontWeight(.medium)
                    }
                    HStack{
                        Text("Skills: ")
                            .foregroundColor(.gray)
                            .font(.title2)
                        Spacer()
                        Text("React JS, Node Js, MongoDB, Expres JS, JavaScript.")
                            .font(.title2)
                            .fontWeight(.medium)
                    }
                    
                    HStack{
                        Text("Hobbies: ")
                            .foregroundColor(.gray)
                            .font(.title2)
                        Spacer()
                        Text("Playing, Reading And Listening.")
                            .font(.title2)
                            .fontWeight(.medium)
                    }
                    Divider()
                    Text("About: ")
                        .font(.title2).bold()
                    Text(landmark.description).font(.title3)
                }
                .padding()
                Spacer()

        }
        .navigationTitle(landmark.name)
        .navigationBarTitleDisplayMode(.inline)
    }
}

struct LandMarkDetails_Previews: PreviewProvider {
    static let modelData = ModelData()
    
    
    static var previews: some View {
        LandMarkDetails(landmark: modelData.landmarks[0])
            .environmentObject(modelData)
    }
}
