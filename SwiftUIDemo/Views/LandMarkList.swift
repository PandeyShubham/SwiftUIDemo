//
//  LandMarkList.swift
//  SwiftUIDemo
//
//  Created by Mobcoder  on 16/08/23.
//

import SwiftUI

struct LandMarkList: View {
    
    @State private var showFavOnly = false;
    @EnvironmentObject var modelData: ModelData
    
    var filteredLandmarks: [Landmark] {
        modelData.landmarks.filter { landmark in
            (!showFavOnly || landmark.isFavorite)
        }
    }
    
    var body: some View {
        NavigationView {
            List {
                Toggle(isOn: $showFavOnly) {
                    Text("Favorites only")
                }
                
                ForEach(filteredLandmarks) { landmark in
                    NavigationLink {
                        LandMarkDetails(landmark: landmark)
                    } label: {
                        LandMarkView(landmark: landmark)
                    }
                }
            }
            .navigationTitle("Landmarks")
        }
    }
}

struct LandMarkList_Previews: PreviewProvider {
    static var previews: some View {
        LandMarkList().environmentObject(ModelData())
    }
}
