//
//  RotateBadge.swift
//  SwiftUIDemo
//
//  Created by Mobcoder  on 18/08/23.
//

import SwiftUI

struct RotateBadge: View {
    let angle: Angle
       
       var body: some View {
           BadgeSymbol()
               .padding(-60)
               .rotationEffect(angle, anchor: .bottom)
       }
}

struct RotateBadge_Previews: PreviewProvider {
    static var previews: some View {
        RotateBadge(angle: Angle(degrees: 0))
    }
}
